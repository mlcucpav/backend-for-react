package com.te.jbc.backendforreact;

import com.te.jbc.backendforreact.dto.RoleDto;
import com.te.jbc.backendforreact.dto.UserDto;
import com.te.jbc.backendforreact.service.RoleService;
import com.te.jbc.backendforreact.service.UserService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Component
public class Initializer implements InitializingBean {

    @Autowired private UserService userService;
    @Autowired private RoleService roleService;

    @Override
    @Transactional
    public void afterPropertiesSet() throws Exception {
//        userService.deleteAll();
//        roleService.deleteAll();
//
//        RoleDto hrRole = roleService.save(RoleDto.builder().name("HR").build());
//        RoleDto managerRole = roleService.save(RoleDto.builder().name("MANAGER").build());
//        RoleDto specialistRole = roleService.save(RoleDto.builder().name("SPECIALIST").build());
//
//        UserDto jana = userService.save(UserDto.builder()
//                .name("Jana Zencova")
//                .email("jana.zencova@tietoevry.com")
//                .password("super-secret")
//                .roles(Set.of(hrRole))
//                .build());
//        UserDto marek = userService.save(UserDto.builder()
//                .name("Marek Popov")
//                .email("marek.popov@tietoevry.com")
//                .password("super-secret")
//                .roles(Set.of(managerRole))
//                .build());
//        UserDto pavel = userService.save(UserDto.builder()
//                .name("Pavel Mlcuch")
//                .email("pavel.mlcuch@tietoevry.com")
//                .password("super-secret")
//                .roles(Set.of(specialistRole))
//                .build());
    }
}
