package com.te.jbc.backendforreact.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class InterviewDto implements Dto {
    private Long id;
    private String actionTaken;
    private String description;

    private CandidateDto candidate;
    private Set<InterviewParticipationDto> interviewParticipations;

}
