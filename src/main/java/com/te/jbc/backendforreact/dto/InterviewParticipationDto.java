package com.te.jbc.backendforreact.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InterviewParticipationDto implements Dto {
    private Long id;
    private String suggestedAction;
    private String comment;

    private InterviewDto interview;
    private UserDto participant;

}
