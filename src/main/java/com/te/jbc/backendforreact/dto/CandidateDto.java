package com.te.jbc.backendforreact.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CandidateDto implements Dto {
    public Long id;
    public String name;
    public String email;
    public String phone;
}
