package com.te.jbc.backendforreact.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class RoleDto implements Dto {
    Long id;
    String name;
}
