package com.te.jbc.backendforreact.dto.enumerated;

public enum InterviewAction {
    HIRE, REJECT;
}
