package com.te.jbc.backendforreact.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Builder
@Data
public class UserDto implements Dto {
    public Long id;
    public String name;
    public String email;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public String password;

    public Set<RoleDto> roles;
}
