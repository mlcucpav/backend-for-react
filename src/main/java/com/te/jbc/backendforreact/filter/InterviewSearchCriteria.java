package com.te.jbc.backendforreact.filter;

import lombok.Data;

@Data
public class InterviewSearchCriteria {
    private String actionTaken;
    private String description;
    private String candidateName;
}
