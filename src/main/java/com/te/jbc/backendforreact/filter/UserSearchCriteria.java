package com.te.jbc.backendforreact.filter;

import lombok.Data;

@Data
public class UserSearchCriteria {
    private String name;
    private String email;
    private String role;
}
