package com.te.jbc.backendforreact.filter;

import lombok.Data;

@Data
public class InterviewParticipationSearchCriteria {
    private String suggestedAction;
    private String comment;
    private String candidateName;
    private String interviewerName;
}
