package com.te.jbc.backendforreact.filter;

import lombok.Data;

@Data
public class CandidateSearchCriteria {
    private String name;
    private String email;
    private String phone;
}
