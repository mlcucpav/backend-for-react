package com.te.jbc.backendforreact.service;

import com.te.jbc.backendforreact.dao.UserRepository;
import com.te.jbc.backendforreact.domain.User;
import com.te.jbc.backendforreact.domain.specification.UserSpecification;
import com.te.jbc.backendforreact.dto.UserDto;
import com.te.jbc.backendforreact.filter.UserSearchCriteria;
import com.te.jbc.backendforreact.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserService extends AbstractService<User, UserDto, UserSearchCriteria> {

    @Autowired private UserRepository userRepository;
    @Autowired private UserSpecification userSpecification;
    @Autowired private UserMapper userMapper;

    public void deleteAll() {
        userRepository.deleteAll();
    }

    @Override
    protected UserRepository getRepository() {
        return userRepository;
    }

    @Override
    protected UserMapper getMapper() {
        return userMapper;
    }

    @Override
    protected List<Optional<Specification<User>>> getSpecification(UserSearchCriteria searchCriteria) {
        return List.of(
                ifNotBlankOrEmpty(searchCriteria.getName(), () -> userSpecification.specificationUserNameLike(searchCriteria.getName())),
                ifNotBlankOrEmpty(searchCriteria.getEmail(), () -> userSpecification.specificationUserEmailLike(searchCriteria.getEmail())),
                ifNotBlankOrEmpty(searchCriteria.getRole(), () -> userSpecification.specificationUserRoleIs(searchCriteria.getRole()))
        );
    }
}
