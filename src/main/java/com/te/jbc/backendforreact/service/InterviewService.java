package com.te.jbc.backendforreact.service;

import com.te.jbc.backendforreact.dao.InterviewRepository;
import com.te.jbc.backendforreact.domain.Interview;
import com.te.jbc.backendforreact.domain.specification.InterviewSpecification;
import com.te.jbc.backendforreact.dto.InterviewDto;
import com.te.jbc.backendforreact.filter.InterviewSearchCriteria;
import com.te.jbc.backendforreact.mapper.InterviewMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class InterviewService extends AbstractService<Interview, InterviewDto, InterviewSearchCriteria> {

    @Autowired private InterviewRepository repository;
    @Autowired private InterviewSpecification specification;
    @Autowired private InterviewMapper mapper;

    @Override
    protected InterviewRepository getRepository() {
        return repository;
    }

    @Override
    protected InterviewMapper getMapper() {
        return mapper;
    }

    @Override
    protected List<Optional<Specification<Interview>>> getSpecification(InterviewSearchCriteria searchCriteria) {
        return List.of(
                ifNotBlankOrEmpty(searchCriteria.getCandidateName(), () -> specification.specificationCandidateNameLike(searchCriteria.getCandidateName())),
                ifNotBlankOrEmpty(searchCriteria.getDescription(), () -> specification.specificationDescriptionLike(searchCriteria.getDescription())),
                ifNotBlankOrEmpty(searchCriteria.getActionTaken(), () -> specification.specificationActionTakenIs(searchCriteria.getActionTaken()))
        );
    }

}
