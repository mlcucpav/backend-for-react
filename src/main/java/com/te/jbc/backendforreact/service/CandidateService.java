package com.te.jbc.backendforreact.service;

import com.te.jbc.backendforreact.dao.CandidateRepository;
import com.te.jbc.backendforreact.domain.Candidate;
import com.te.jbc.backendforreact.domain.specification.CandidateSpecification;
import com.te.jbc.backendforreact.dto.CandidateDto;
import com.te.jbc.backendforreact.filter.CandidateSearchCriteria;
import com.te.jbc.backendforreact.mapper.CandidateMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CandidateService extends AbstractService<Candidate, CandidateDto, CandidateSearchCriteria> {

    @Autowired private CandidateRepository repository;
    @Autowired private CandidateSpecification specification;
    @Autowired private CandidateMapper mapper;

    @Override
    protected CandidateRepository getRepository() {
        return repository;
    }

    @Override
    protected CandidateMapper getMapper() {
        return mapper;
    }

    @Override
    protected List<Optional<Specification<Candidate>>> getSpecification(CandidateSearchCriteria searchCriteria) {
        return List.of(
                ifNotBlankOrEmpty(searchCriteria.getEmail(), () -> specification.specificationCandidateEmailLike(searchCriteria.getEmail())),
                ifNotBlankOrEmpty(searchCriteria.getName(), () -> specification.specificationCandidateNameLike(searchCriteria.getName())),
                ifNotBlankOrEmpty(searchCriteria.getPhone(), () -> specification.specificationCandidatePhoneLike(searchCriteria.getPhone()))
        );
    }

}
