package com.te.jbc.backendforreact.service;

import com.te.jbc.backendforreact.dao.InterviewParticipationRepository;
import com.te.jbc.backendforreact.domain.InterviewParticipation;
import com.te.jbc.backendforreact.domain.specification.InterviewParticipationSpecification;
import com.te.jbc.backendforreact.dto.InterviewParticipationDto;
import com.te.jbc.backendforreact.filter.InterviewParticipationSearchCriteria;
import com.te.jbc.backendforreact.mapper.InterviewParticipationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class InterviewParticipationService extends AbstractService<InterviewParticipation, InterviewParticipationDto, InterviewParticipationSearchCriteria> {

    @Autowired private InterviewParticipationRepository repository;
    @Autowired private InterviewParticipationSpecification specification;
    @Autowired private InterviewParticipationMapper mapper;

    @Override
    protected InterviewParticipationRepository getRepository() {
        return repository;
    }

    @Override
    protected InterviewParticipationMapper getMapper() {
        return mapper;
    }

    @Override
    protected List<Optional<Specification<InterviewParticipation>>> getSpecification(InterviewParticipationSearchCriteria searchCriteria) {
        return List.of(
                ifNotBlankOrEmpty(searchCriteria.getCandidateName(), () -> specification.specificationCandidateNameLike(searchCriteria.getCandidateName())),
                ifNotBlankOrEmpty(searchCriteria.getComment(), () -> specification.specificationCommentLike(searchCriteria.getComment())),
                ifNotBlankOrEmpty(searchCriteria.getInterviewerName(), () -> specification.specificationInterviewerNameLike(searchCriteria.getInterviewerName())),
                ifNotBlankOrEmpty(searchCriteria.getSuggestedAction(), () -> specification.specificationSuggestedActionIs(searchCriteria.getSuggestedAction()))
        );
    }

}
