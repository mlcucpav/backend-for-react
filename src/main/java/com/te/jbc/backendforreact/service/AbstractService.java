package com.te.jbc.backendforreact.service;

import com.te.jbc.backendforreact.exception.NotFoundException;
import com.te.jbc.backendforreact.mapper.AbstractMapper;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

public abstract class AbstractService<DOMAIN, DTO, SC> {

    protected abstract <R extends JpaRepository<DOMAIN, Long> & JpaSpecificationExecutor<DOMAIN>> R getRepository();
    protected abstract AbstractMapper<DOMAIN, DTO> getMapper();

    protected Optional<Specification<DOMAIN>> ifNotBlankOrEmpty(String str, Supplier<Specification<DOMAIN>> specSupplier) {
        return str != null && !str.isBlank() ? Optional.ofNullable(specSupplier.get()) : Optional.empty();
    }

    public DTO save(DTO dto) {
        return getMapper().toDto().apply(
                getRepository().save(
                        getMapper().toDomain().apply(dto)
                )
        );
    }

    @Transactional(readOnly = true)
    public DTO get(Long id) {
        DOMAIN domain = getRepository().findById(id).orElseThrow(NotFoundException::new);
        return getMapper().toDto().apply(domain);
    }

    protected abstract List<Optional<Specification<DOMAIN>>> getSpecification(SC searchCriteria);

    @Transactional(readOnly = true)
    public Set<DTO> search(SC searchCriteria) {
        return getMapper().toSetOfDtos().apply(
                getRepository().findAll(
                        joinSpecification(
                                getSpecification(searchCriteria)
                        )
                )
        );
    }

    public void delete(Long id) {
        getRepository().deleteById(id);
    }

    private Specification<DOMAIN> joinSpecification(List<Optional<Specification<DOMAIN>>> specifications) {
        AtomicReference<Specification<DOMAIN>> specification = new AtomicReference<>();

        specification.set(
                specifications.stream()
                        .filter(Optional::isPresent)
                        .findFirst()
                        .map(Optional::get)
                        .map(Specification::where)
                        .orElse(null)
        );

        specifications.stream().skip(1)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(spec -> specification.set(specification.get().or(spec)));

        return specification.get();
    }

}
