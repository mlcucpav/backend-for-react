package com.te.jbc.backendforreact.service;

import com.te.jbc.backendforreact.dao.RoleRepository;
import com.te.jbc.backendforreact.dto.RoleDto;
import com.te.jbc.backendforreact.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RoleService {

    @Autowired private RoleRepository roleRepository;
    @Autowired private RoleMapper roleMapper;

    public void deleteAll() {
        roleRepository.deleteAll();
    }

    public RoleDto save(RoleDto role) {
        return roleMapper.toDto().apply(roleRepository.save(roleMapper.toDomain().apply(role)));
    }

    @Transactional(readOnly = true) public RoleDto get(Long id) {
        return roleMapper.toDto().apply(roleRepository.getById(id));
    }

}
