package com.te.jbc.backendforreact.exception;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@Aspect
public class ExceptionHandlerAspect {

    private static final String DUPLICATE_IN_UNIQUE = "ERROR: duplicate key value violates unique constraint \"([^\"]+)\"\n  Detail: Key \\(([^\"]+)\\)=\\(([^\"]+)\\) already exists.";

    @Around("@annotation(com.te.jbc.backendforreact.exception.HandleExceptions)")
    public Object mainHandler(ProceedingJoinPoint joinPoint) throws Throwable {
        try {
            return joinPoint.proceed();
        } catch (DataIntegrityViolationException e) {
            checkDuplicateInUnique(e.getCause().getCause().getMessage());
        } catch (ConstraintViolationException e) {
            checkDuplicateInUnique(e.getCause().getMessage());
        } catch (SQLException e) {
            checkDuplicateInUnique(e.getMessage());
        }
        return null;
    }

    private void checkDuplicateInUnique(String message) {
        Matcher m = Pattern.compile(DUPLICATE_IN_UNIQUE).matcher(message);
        if (m.matches()) {
            throw new DuplicateInUniqueException(m.group(2), m.group(3));
        }
    }
}
