package com.te.jbc.backendforreact.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class DuplicateInUniqueException extends RuntimeException {
    public DuplicateInUniqueException(String column, String value) {
        super(String.format("%s with value of %s already exists", column, value));
    }
}
