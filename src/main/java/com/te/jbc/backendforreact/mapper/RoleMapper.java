package com.te.jbc.backendforreact.mapper;

import com.te.jbc.backendforreact.domain.Role;
import com.te.jbc.backendforreact.dto.RoleDto;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class RoleMapper extends AbstractMapper<Role, RoleDto> {
    @Override
    public Function<Role, RoleDto> toDto() {
        return role -> RoleDto.builder().id(role.getId()).name(role.getName()).build();
    }

    @Override
    public Function<RoleDto, Role> toDomain() {
        return dto -> Role.builder().name(dto.getName()).id(dto.getId()).build();
    }
}
