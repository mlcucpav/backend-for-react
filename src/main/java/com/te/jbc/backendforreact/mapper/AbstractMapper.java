package com.te.jbc.backendforreact.mapper;

import java.util.Collection;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class AbstractMapper<DOMAIN, DTO> {
    public abstract Function<DOMAIN, DTO> toDto();
    public abstract Function<DTO, DOMAIN> toDomain();

    public Function<Collection<DOMAIN>, Set<DTO>> toSetOfDtos() {
        return domainObjects -> domainObjects == null ? Set.of() : domainObjects.stream().map(toDto()).collect(Collectors.toSet());
    }
}
