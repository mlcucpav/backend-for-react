package com.te.jbc.backendforreact.mapper;

import com.te.jbc.backendforreact.dao.InterviewParticipationRepository;
import com.te.jbc.backendforreact.domain.Interview;
import com.te.jbc.backendforreact.domain.InterviewParticipation;
import com.te.jbc.backendforreact.dto.InterviewDto;
import com.te.jbc.backendforreact.dto.InterviewParticipationDto;
import com.te.jbc.backendforreact.dto.enumerated.InterviewAction;
import com.te.jbc.backendforreact.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class InterviewParticipationMapper extends AbstractMapper<InterviewParticipation, InterviewParticipationDto> {

    @Autowired UserMapper userMapper;
    @Autowired InterviewMapper interviewMapper;

    @Autowired InterviewParticipationRepository interviewParticipationRepository;

    @Override
    public Function<InterviewParticipation, InterviewParticipationDto> toDto() {
        return domain -> InterviewParticipationDto.builder()
                .id(domain.getId())
                .suggestedAction(domain.getSuggestedAction().name())
                .comment(domain.getComment())
                .interview(domain.getInterview() == null ? null : InterviewDto.builder().id(domain.getInterview().getId()).build())
                .participant(userMapper.toDto().apply(domain.getParticipant()))
                .build();
    }

    @Override
    public Function<InterviewParticipationDto, InterviewParticipation> toDomain() {
        return dto -> InterviewParticipation.builder()
                                .id(dto.getId())
                                .suggestedAction(InterviewAction.valueOf(dto.getSuggestedAction()))
                                .comment(dto.getComment())
                                .interview(interviewMapper.toDomain().apply(dto.getInterview()))
                                .participant(userMapper.toDomain().apply(dto.getParticipant()))
                                .build();
    }
}
