package com.te.jbc.backendforreact.mapper;

import com.te.jbc.backendforreact.dao.RoleRepository;
import com.te.jbc.backendforreact.dao.UserRepository;
import com.te.jbc.backendforreact.domain.User;
import com.te.jbc.backendforreact.dto.RoleDto;
import com.te.jbc.backendforreact.dto.UserDto;
import com.te.jbc.backendforreact.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class UserMapper extends AbstractMapper<User, UserDto> {

    @Autowired UserRepository userRepository;
    @Autowired RoleMapper roleMapper;
    @Autowired RoleRepository roleRepository;
    @Autowired PasswordEncoder  passwordEncoder;

    @Override
    public Function<User, UserDto> toDto() {
        return user -> UserDto.builder()
                .id(user.getId())
                .name(user.getName())
                .email(user.getEmail())
                .password(user.getPassword())
                .roles(roleMapper.toSetOfDtos().apply(user.getRoles()))
                .build();
    }
    @Override
    public Function<UserDto, User> toDomain() {
        return dto -> User.builder()
                                .id(dto.getId())
                                .name(dto.getName())
                                .email(dto.getEmail())
                                .password(passwordEncoder.encode(dto.getPassword()))
                                .roles(roleRepository.findAllByNameIn(dto.getRoles().stream().map(RoleDto::getName).collect(Collectors.toSet())))
                                .build();
    }
}
