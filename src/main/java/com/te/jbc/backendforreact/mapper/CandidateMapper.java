package com.te.jbc.backendforreact.mapper;

import com.te.jbc.backendforreact.dao.CandidateRepository;
import com.te.jbc.backendforreact.domain.Candidate;
import com.te.jbc.backendforreact.dto.CandidateDto;
import com.te.jbc.backendforreact.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class CandidateMapper extends AbstractMapper<Candidate, CandidateDto> {

    @Autowired CandidateRepository candidateRepository;

    @Override
    public Function<Candidate, CandidateDto> toDto() {
        return candidate -> CandidateDto.builder()
                .id(candidate.getId())
                .name(candidate.getName())
                .email(candidate.getEmail())
                .phone(candidate.getPhone())
                .build();
    }

    @Override
    public Function<CandidateDto, Candidate> toDomain() {
        return dto -> Candidate.builder()
                .id(dto.getId())
                .name(dto.getName())
                .email(dto.getEmail())
                .phone(dto.getPhone())
                .build();
    }
}
