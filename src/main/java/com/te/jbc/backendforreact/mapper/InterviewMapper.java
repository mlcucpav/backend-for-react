package com.te.jbc.backendforreact.mapper;

import com.te.jbc.backendforreact.dao.InterviewParticipationRepository;
import com.te.jbc.backendforreact.dao.InterviewRepository;
import com.te.jbc.backendforreact.domain.Interview;
import com.te.jbc.backendforreact.dto.InterviewDto;
import com.te.jbc.backendforreact.dto.enumerated.InterviewAction;
import com.te.jbc.backendforreact.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class InterviewMapper extends AbstractMapper<Interview, InterviewDto> {

    @Autowired CandidateMapper candidateMapper;
    @Autowired InterviewRepository interviewRepository;
    @Autowired InterviewParticipationMapper interviewParticipationMapper;

    @Override
    public Function<Interview, InterviewDto> toDto() {
        return domain -> InterviewDto.builder()
                .id(domain.getId())
                .actionTaken(domain.getActionTaken() == null ? null : domain.getActionTaken().name())
                .description(domain.getDescription())
                .candidate(candidateMapper.toDto().apply(domain.getCandidate()))
                .interviewParticipations(
                        interviewParticipationMapper.toSetOfDtos().apply(
                                domain.getInterviewParticipations()
                        )
                )
                .build();
    }

    @Override
    public Function<InterviewDto, Interview> toDomain() {
        return dto -> Interview.builder()
                .id(dto.getId())
                .actionTaken(dto.getActionTaken() == null ? null : InterviewAction.valueOf(dto.getActionTaken()))
                .description(dto.getDescription())
                .candidate(candidateMapper.toDomain().apply(dto.getCandidate()))
                .build();
    }
}
