package com.te.jbc.backendforreact.domain;

import com.te.jbc.backendforreact.dto.enumerated.InterviewAction;
import lombok.*;

import javax.persistence.*;

@Entity(name = "interview_participation")
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(of = "id")
@ToString(of = {"id", "interview", "participant"})
public class InterviewParticipation {
    @SequenceGenerator(name="interview_participation_seq", sequenceName = "interview_participation_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "interview_participation_seq")
    @Id
    private Long id;
    private InterviewAction suggestedAction;
    private String comment;

    @ManyToOne
    @JoinColumn(name = "interview_id")
    private Interview interview;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User participant;

}
