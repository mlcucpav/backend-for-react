package com.te.jbc.backendforreact.domain;

import com.te.jbc.backendforreact.dto.enumerated.InterviewAction;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "interviews")
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(of = "id")
@ToString(of = {"id", "candidate"})
public class Interview {
    @SequenceGenerator(name="interviews_seq", sequenceName = "interviews_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "interviews_seq")
    @Id
    private Long id;
    @Enumerated(EnumType.STRING)
    private InterviewAction actionTaken;
    private String description;

    @ManyToOne
    @JoinColumn(name = "candidate_id")
    private Candidate candidate;

    @OneToMany(mappedBy = "interview")
    private Set<InterviewParticipation> interviewParticipations;
}
