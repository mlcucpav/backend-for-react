package com.te.jbc.backendforreact.domain.specification;

import com.te.jbc.backendforreact.domain.Candidate;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
public class CandidateSpecification extends AbstractSpecification<Candidate> {

    public String candidateName() { return "name"; }
    public String candidateEmail() { return "email"; }
    public String candidatePhone() { return "phone"; }

    public Specification<Candidate> specificationCandidateEmailLike(String email) {
        return specificationLike(candidateEmail(), email);
    }
    public Specification<Candidate> specificationCandidateNameLike(String name) {
        return specificationLike(candidateName(), name);
    }
    public Specification<Candidate> specificationCandidatePhoneLike(String phone) {
        return specificationLike(candidatePhone(), phone);
    }
}
