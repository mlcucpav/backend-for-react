package com.te.jbc.backendforreact.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "candidates")
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(of = "id")
@ToString(of = "name")
public class Candidate {
    @SequenceGenerator(name="candidates_seq", sequenceName = "candidates_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "candidates_seq")
    @Id
    private Long id;
    private String name;
    private String email;
    private String phone;

    @OneToMany(mappedBy = "candidate")
    private Set<Interview> interviews;
}
