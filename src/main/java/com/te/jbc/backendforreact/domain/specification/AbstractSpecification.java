package com.te.jbc.backendforreact.domain.specification;

import org.springframework.data.jpa.domain.Specification;

public abstract class AbstractSpecification<T> {

    public Specification<T> specificationIs(String field, Enum<?> value) {
        return (entity, cq, cb) -> cb.equal(entity.get(field), value);
    }

    public Specification<T> specificationLike(String field, String value) {
        return (entity, cq, cb) -> cb.like(cb.lower(entity.get(field)), String.format("%%%s%%", value).toLowerCase());
    }

}
