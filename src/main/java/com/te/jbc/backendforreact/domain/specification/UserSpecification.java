package com.te.jbc.backendforreact.domain.specification;

import com.te.jbc.backendforreact.domain.Role;
import com.te.jbc.backendforreact.domain.User;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Join;

@Component
public class UserSpecification extends AbstractSpecification<User> {

    public String userEmail() { return "email"; }
    public String userName() { return "name"; }

    public Specification<User> specificationUserEmailLike(String email) {
        return specificationLike(userEmail(), email);
    }
    public Specification<User> specificationUserNameLike(String name) {
        return specificationLike(userName(), name);
    }

    public Specification<User> specificationUserRoleIs(String roleName) {
        return (entity, cq, cb) -> {
            Join<User, Role> roleJoin = entity.join("roles");
            return cb.equal(roleJoin.get("name"), roleName);
        };
    }

}
