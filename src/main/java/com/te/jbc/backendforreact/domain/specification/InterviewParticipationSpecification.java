package com.te.jbc.backendforreact.domain.specification;

import com.te.jbc.backendforreact.domain.Candidate;
import com.te.jbc.backendforreact.domain.Interview;
import com.te.jbc.backendforreact.domain.InterviewParticipation;
import com.te.jbc.backendforreact.domain.User;
import com.te.jbc.backendforreact.dto.enumerated.InterviewAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Join;

@Component
public class InterviewParticipationSpecification extends AbstractSpecification<InterviewParticipation> {

    @Autowired
    private CandidateSpecification candidateSpecification;
    @Autowired
    private UserSpecification userSpecification;

    public String interviewSuggestedAction() { return "suggestedAction"; }
    public String interviewComment() { return "comment"; }

    public Specification<InterviewParticipation> specificationSuggestedActionIs(String action) {
        return specificationIs(interviewSuggestedAction(), InterviewAction.valueOf(action));
    }
    public Specification<InterviewParticipation> specificationCommentLike(String search) {
        return specificationLike(interviewComment(), search);
    }

    public Specification<InterviewParticipation> specificationCandidateNameLike(String name) {
        return (entity, cq, cb) -> {
            Join<InterviewParticipation, Interview> interviewJoin = entity.join("interview");
            Join<Interview, Candidate> candidateJoin = interviewJoin.join("candidate");
            return cb.like(candidateJoin.get(candidateSpecification.candidateName()), String.format("%%%s%%", name));
        };
    }

    public Specification<InterviewParticipation> specificationInterviewerNameLike(String name) {
        return (entity, cq, cb) -> {
            Join<Interview, User> userJoin = entity.join("participant");
            return cb.like(userJoin.get(userSpecification.userName()), String.format("%%%s%%", name));
        };
    }

}
