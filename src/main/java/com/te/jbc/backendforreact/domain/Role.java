package com.te.jbc.backendforreact.domain;

import lombok.*;

import javax.persistence.*;

@Entity(name = "roles")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(of = "id")
public class Role {
    @SequenceGenerator(name="roles_seq", sequenceName = "roles_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "roles_seq")
    @Id
    private Long id;
    private String name;
}
