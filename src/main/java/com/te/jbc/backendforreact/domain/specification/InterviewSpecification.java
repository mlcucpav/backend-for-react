package com.te.jbc.backendforreact.domain.specification;

import com.te.jbc.backendforreact.domain.Candidate;
import com.te.jbc.backendforreact.domain.Interview;
import com.te.jbc.backendforreact.dto.enumerated.InterviewAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Join;

@Component
public class InterviewSpecification extends AbstractSpecification<Interview> {

    @Autowired private CandidateSpecification candidateSpecification;

    public String interviewType() { return "type"; }
    public String interviewActionTaken() { return "actionTaken"; }
    public String interviewDescription() { return "description"; }

    public Specification<Interview> specificationActionTakenIs(String action) {
        return specificationIs(interviewActionTaken(), InterviewAction.valueOf(action));
    }
    public Specification<Interview> specificationDescriptionLike(String search) {
        return specificationLike(interviewDescription(), search);
    }

    public Specification<Interview> specificationCandidateNameLike(String name) {
        return (entity, cq, cb) -> {
            Join<Interview, Candidate> candidateJoin = entity.join("candidate");
            return cb.like(candidateJoin.get(candidateSpecification.candidateName()), String.format("%%%s%%", name));
        };
    }

}
