package com.te.jbc.backendforreact.controller;

import com.te.jbc.backendforreact.dto.InterviewParticipationDto;
import com.te.jbc.backendforreact.filter.InterviewParticipationSearchCriteria;
import com.te.jbc.backendforreact.service.InterviewParticipationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Addresses.INTERVIEW_PARTICIPATIONS)
public class InterviewParticipationController extends AbstractController<InterviewParticipationDto, InterviewParticipationSearchCriteria> {

    @Autowired private InterviewParticipationService interviewParticipationService;

    @Override
    protected InterviewParticipationService getService() {
        return interviewParticipationService;
    }

}
