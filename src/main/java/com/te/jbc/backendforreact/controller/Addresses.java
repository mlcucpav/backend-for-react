package com.te.jbc.backendforreact.controller;

public interface Addresses {

    String PREFIX = "/api/v1";

    String USERS = PREFIX + "/users";
    String CANDIDATES = PREFIX + "/candidates";
    String INTERVIEWS = PREFIX + "/interviews";
    String INTERVIEW_PARTICIPATIONS = PREFIX + "/interview-participations";
}
