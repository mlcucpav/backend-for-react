package com.te.jbc.backendforreact.controller;

import com.te.jbc.backendforreact.dto.UserDto;
import com.te.jbc.backendforreact.filter.UserSearchCriteria;
import com.te.jbc.backendforreact.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Addresses.USERS)
public class UserController extends AbstractController<UserDto, UserSearchCriteria> {

    @Autowired private UserService userService;

    @Override
    protected UserService getService() {
        return userService;
    }

}
