package com.te.jbc.backendforreact.controller;

import com.te.jbc.backendforreact.dto.CandidateDto;
import com.te.jbc.backendforreact.filter.CandidateSearchCriteria;
import com.te.jbc.backendforreact.service.CandidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Addresses.CANDIDATES)
public class CandidateController extends AbstractController<CandidateDto, CandidateSearchCriteria> {

    @Autowired
    private CandidateService candidateService;

    @Override
    protected CandidateService getService() {
        return candidateService;
    }

}
