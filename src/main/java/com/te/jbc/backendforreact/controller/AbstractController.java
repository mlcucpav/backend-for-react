package com.te.jbc.backendforreact.controller;

import com.te.jbc.backendforreact.dto.Dto;
import com.te.jbc.backendforreact.exception.HandleExceptions;
import com.te.jbc.backendforreact.service.AbstractService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Set;

public abstract class AbstractController<DTO extends Dto, SC> {

    protected abstract AbstractService<?, DTO, SC> getService();

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/{id}") public DTO get(@PathVariable Long id) {
        return getService().get(id);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping public Set<DTO> search(SC userSearchCriteria) {
        return getService().search(userSearchCriteria);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @HandleExceptions
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public DTO save(@RequestBody DTO user) {
        return getService().save(user);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @HandleExceptions
    @PutMapping("/{id}")
    public DTO update(@PathVariable Long id, @RequestBody DTO userDto) {
        userDto.setId(id);
        return getService().save(userDto);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @HandleExceptions
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        getService().delete(id);
    }

}
