package com.te.jbc.backendforreact.controller;

import com.te.jbc.backendforreact.dto.InterviewDto;
import com.te.jbc.backendforreact.filter.InterviewSearchCriteria;
import com.te.jbc.backendforreact.service.InterviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Addresses.INTERVIEWS)
public class InterviewController extends AbstractController<InterviewDto, InterviewSearchCriteria> {

    @Autowired private InterviewService interviewService;

    @Override
    protected InterviewService getService() {
        return interviewService;
    }

}
