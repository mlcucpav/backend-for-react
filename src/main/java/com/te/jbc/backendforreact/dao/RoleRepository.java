package com.te.jbc.backendforreact.dao;

import com.te.jbc.backendforreact.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Set<Role> findAllByNameIn(Iterable<String> names);
}
