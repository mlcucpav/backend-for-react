package com.te.jbc.backendforreact.dao;

import com.te.jbc.backendforreact.domain.InterviewParticipation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Set;

public interface InterviewParticipationRepository extends JpaRepository<InterviewParticipation, Long>, JpaSpecificationExecutor<InterviewParticipation> {
    Set<InterviewParticipation> findByInterviewId(Long interviewId);
}
