CREATE TABLE users
(
    id bigserial primary key,
    email text not null,
    password text not null,
    name text
);

CREATE TABLE roles
(
    id bigserial primary key,
    name text not null
);

CREATE TABLE user_role
(
    user_id bigint references users (id),
    role_id bigint references roles (id)
);

CREATE TABLE candidates
(
    id bigserial primary key,
    name text not null,
    email text not null,
    phone text not null
);

CREATE TABLE interviews
(
    id bigserial primary key,
    type text not null,
    candidate_id bigint not null references candidates (id),
    action_taken text,
    description text
);

CREATE TABLE interview_participation
(
    interview_id bigint references interviews (id),
    user_id bigint references users (id),
    suggested_action text,
    comment text
);
